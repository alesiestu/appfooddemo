import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:foodapp/track/lavorazione/home.dart';
import 'package:flutter/services.dart';


class lavorazione extends StatefulWidget{
  lavorazione({this.prodotto});
  final String prodotto;
  
  @override
  _lavorazione createState() => new _lavorazione();

}


class _lavorazione extends State<lavorazione>{

 String prodotto;
 

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    prodotto=widget.prodotto;

  }

  


@override
    Widget build(BuildContext context) {
    return Scaffold(
      


      appBar: new AppBar(
        title: Text('Materie prime'),
    
        actions: <Widget>[


          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
    
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("ingresso").where("abilitato",isEqualTo: "0")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new ListaProdotti(document: snapshot.data.documents,prodotto: prodotto,);
      },

      
      
     ),
    
      
     ]
    )




    );
    }


}


  
class ListaProdotti extends StatefulWidget{
  final String prodotto;

  
  ListaProdotti({this.document, this.prodotto});
  final List<DocumentSnapshot> document;
  @override
  _ListaProdottiState createState() => new _ListaProdottiState();
  
  
}

class _ListaProdottiState extends State<ListaProdotti>{
  DateTime _datacorrente= new DateTime.now();

  TextEditingController controlleraggiunto;
  String stato='';
  String _testodata='';
  TextEditingController _c ;
  String _text='';
  int carrello=0;
  
  void incremento(){
    setState(() {
          carrello++;
        }); 
  }
  

  List<DocumentSnapshot> document;
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      
      _testodata="${_datacorrente.day}/${_datacorrente.month}/${_datacorrente.year} ";
        SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitUp,
  ]);
    }

  Future<Null> _selezionadata(BuildContext context) async{
   final picked= await showDatePicker(
     context: context,
     initialDate: _datacorrente,
     firstDate: DateTime(2018),
     lastDate: DateTime(2080)
   );

   if(picked!= null){
     setState(() {
        _datacorrente=picked;
        _testodata="${picked.day}/${picked.month}/${picked.year} ";
        _c = new TextEditingController();
        //_testodata2="${picked.day}/${picked.month}/${picked.year}/${picked.hour}/${picked.minute}/${picked.second} ";
          });
   }
  }



  void _addData(String etichetta, String urlimmagine, String nomefornitore, String dataingresso, String ivafornitore, String quantita, String note){ 
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
         "datalavorazione" : _testodata,
         "pathimmagine": urlimmagine, 
         "etichetta":etichetta,   
         "nomefornitore":nomefornitore,
         "dataingresso":dataingresso,
         "ivafornitore":ivafornitore,
         "quantita":quantita,
         "note":note,
       });

     }); 
     Navigator.pop(context);
  }



   @override
   Widget build(BuildContext context){
   return Scaffold(
   body:
   ListView.builder(
    padding: const EdgeInsets.all(4.0), 
    itemCount: document.length,
    itemBuilder: (BuildContext context, int i) { 
       String urlimmagine=document[i].data['pathimmagine'].toString();
       String etichetta= document[i].data['etichetta'].toString();
       String ivafornitore= document[i].data['ivafornitore'].toString();
       String quantita=document[i].data['quantita'].toString();
       String note=document[i].data['quantita'].toString();
       String nomefornitore= document[i].data['nomefornitore'].toString();
       String datafornitura= document[i].data['data'].toString();     
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: new Card (
          child:
               Column(
                 children: <Widget>[
                   ListTile( 
                    title: 
                      Row(
                        children: <Widget>[
                          Column(children: <Widget>[
                          Image.network(urlimmagine,width: 100,height: 130),
                    ],),
                    Padding(
                      padding: const EdgeInsets.only(left:16.0),
                      child: Column(
                              children: <Widget>[
                                Text('Nome etichetta: '),
                                Text(etichetta),
                                Padding(
                                  padding: const EdgeInsets.only(top:4.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text('Data fornitura '),  
                                    ],
                                  ),
                                ),
                                Text(datafornitura),
                                
                              ],
                            ),
                    ),
                        ],
                      ) 
              ),
               new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    FlatButton(
                      onPressed: (){
                        showDialog<String>(context: context,
                        builder: (BuildContext context)=>AlertDialog(
                        title: Text('Confermi esaurimento scorte di $etichetta?'),
                        actions: <Widget>[
                          FlatButton(
                            onPressed: (){
                              Navigator.pop(context);
                            },
                            child: Row(
                              children: <Widget>[
                                Text('No')
                              ],
                            ),
                          ),
                          FlatButton(
                            onPressed: (){
                            
                            Firestore.instance.runTransaction((Transaction transaction)async {
                            DocumentSnapshot snapshot=
                            await transaction.get(document[i].reference);
                            await transaction.update(snapshot.reference, {
                              "abilitato": "1",
                            });
                            Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new lavorazione(),
                        ),
                        ModalRoute.withName('/'));


                            });
                             Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new lavorazione(),
                        ),
                        ModalRoute.withName('/'));

                        
                            },
                            child: Row(
                              children: <Widget>[
                                Text('Si')
                              ],
                            ),
                          )
                          
                          
                        ],

                        )
                        );

                      },
                      child: Row(
                        children: <Widget>[
                        Icon(Icons.delete, color: Colors.red,),
                        Text('Prodotto esaurito',style: TextStyle(fontSize: 18),)
                         ],
                      ) ,
                    ),
                    FlatButton(
                      child: Row( children: <Widget>[
                        Icon(Icons.play_for_work),
                        Text('Aggiungi',style: TextStyle(fontSize: 18),)
                      ], ),
                      onPressed: () {
                        
                        showDialog<String>(context: context,
                        builder: (BuildContext context)=>
                              AlertDialog(
                              title: Row(crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
                                Text('Confermi la scelta?')
                              ],),
                              
                              content: Container(
                              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                                        Text('Fornito da:'),
                                        Text('$nomefornitore',style: new TextStyle(fontWeight: FontWeight.bold)),
                                        Padding(
                                          padding: const EdgeInsets.only(top:8.0),
                                          child: Text('In data :'),
                                        ),
                                        Text('$datafornitura')
                                    
                              ],),
                              
                              ),
                              actions: <Widget>[
                                FlatButton(
                                    child: Text('No'),
                                    onPressed: () => setState(() {
                                          Navigator.pop(context);
                                        })),
                                        FlatButton(
                                    child: Text('Si'),
                                    onPressed: () => setState(() {
                                          incremento();
                                        }))
                              ],
                            ));
                          
                          
                          
                       
          

                      },
                    ),
                    
                  ],
                ),
              ),
                 ],
               ),      
          ),
      );

     }

  ),

  //barra inferiore 

  bottomNavigationBar: BottomAppBar(
      child: Container(height: 50.0,
      child: Padding(
        padding: const EdgeInsets.only(left:12.0),
        child: Row(
          children: <Widget>[
            new Text("Materie messe in lavorazione: $carrello"),
           
          ],
        ),
      ),
      ),
    ),
    floatingActionButton: FloatingActionButton(
      onPressed: (){},
      tooltip: 'Increment Counter',
      child: Icon(Icons.save),
    ),
    floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
     );
 
 
  }
  _lavorazione createState() => new _lavorazione();
  
}
class MyDialog extends StatefulWidget {
 MyDialog({this.datafornitura,this.urlimmagine,this.etichetta,this.ivafornitore,this.quantita,this.nomefornitore,this.note});

 final String urlimmagine;
 final String etichetta;
 final String ivafornitore;
 final String quantita;
 final String note;
 final String nomefornitore;
 final String datafornitura;

  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  String urlimmagine;
  String etichetta;
  String ivafornitore;
  String quantita;
  String note;
  String nomefornitore;
  String datafornitura;
  
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      urlimmagine=widget.urlimmagine;
      etichetta=widget.etichetta;
      ivafornitore=widget.ivafornitore;
      quantita=widget.quantita;
      note=widget.note;
      nomefornitore=widget.nomefornitore;
      datafornitura=widget.datafornitura;
    }
  
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(crossAxisAlignment: CrossAxisAlignment.center,mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Text('Confermi la scelta?')
      ],),
      
      content: Container(
       child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Text('Fornito da:'),
                Text('$nomefornitore',style: new TextStyle(fontWeight: FontWeight.bold)),
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: Text('In data :'),
                ),
                Text('$datafornitura')
            
       ],),
       
      ),
      actions: <Widget>[
        FlatButton(
            child: Text('No'),
            onPressed: () => setState(() {
                  
                })),
                FlatButton(
            child: Text('Si'),
            onPressed: () => setState(() {
                  
                }))
      ],
    );
  }
  
}






