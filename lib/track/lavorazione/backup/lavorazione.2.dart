import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:async';
import 'package:foodapp/track/lavorazione/test2.dart';
import 'dart:math';

import 'package:foodapp/track/lavorazione/util/lavorazionemodel.dart';
import 'package:foodapp/track/lavorazione/service/firestoredataservice.dart';

class lavorazione extends StatefulWidget{
  lavorazione({this.nomeprodotto,this.codicesessione});
  final String nomeprodotto;
  final int codicesessione;
  
  @override
  _lavorazione createState() => new _lavorazione();

}


class _lavorazione extends State<lavorazione>{
var codicesessione;
String nomeprodotto;
List<DocumentSnapshot> document1;
List<DocumentSnapshot> document2;
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    nomeprodotto=widget.nomeprodotto;
    codicesessione=widget.codicesessione;

  }

  


@override
    Widget build(BuildContext context) {
    return Scaffold(
      


      appBar: new AppBar(
        title: Row(
          children: <Widget>[
            Text('$codicesessione')
          ],
        ),

    
        actions: <Widget>[


          
        ],
      ),

       
     

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("ingresso")
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
       document1=snapshot.data.documents;
      //return new ListaProdotti(document: snapshot.data.documents,nomeprodotto:nomeprodotto);
      },

      
      
     ),
     StreamBuilder(
       stream: Firestore.instance
       .collection("lavorazione").where("codicesessione", isEqualTo: codicesessione)
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1 ){
        if(!snapshot1.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      List<DocumentSnapshot> document2=snapshot1.data.documents;
      return new ListaProdotti(document: document1,document2:document2,codicesessione:codicesessione);
      },

      
      
     ),

    
      
     ]
    )




    );
    }


}


  
class ListaProdotti extends StatefulWidget{
  final String nomeprodotto;
  final codicesessione;
  
  
  
  ListaProdotti({this.document,this.nomeprodotto,this.document2,this.codicesessione});
  final List<DocumentSnapshot> document;
  final List<DocumentSnapshot> document2;
  @override
  _ListaProdottiState createState() => new _ListaProdottiState();
  
  
}

class _ListaProdottiState extends State<ListaProdotti>{
  DateTime _datacorrente= new DateTime.now();
  String nomeprodotto;

  var codicesesione;

   FirebaseFirestoreService db = new FirebaseFirestoreService();
  

  List<DocumentSnapshot> document;
  List<DocumentSnapshot> document2;
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      document=widget.document;
      nomeprodotto=widget.nomeprodotto;
      document2=widget.document2;
      codicesesione=widget.codicesessione;
    }

  void updateclienti(){  //in caso di eliminazione
  
    Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(selectProduct[0]);
      await transaction.update(snapshot.reference, {
        "abilitato": "test concluso",
      

      });
    });
    Navigator.pop(context);

  }


  void _addData(){
      List myList = [selectProduct, fornitori,immagini];  
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
         "data" : _datacorrente,
        // "codiceingresso": myList[0],
        // "fornitore": myList[1],
         "materieprime": selectProduct,
         "nomeprodotto":nomeprodotto
       });
     });    
     Navigator.pop(context);
  }

  void adddatacheck(String etichetta,String pathimmagine, String fornitore, String iva, String quantita, String note,String data ){
       
       
       Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('lavorazione');
       await reference.add({
        
        "data" : _datacorrente, 
        "materieprime": etichetta,
        "nomeprodotto":nomeprodotto,
        "pathimmagine":pathimmagine,
        "nomefornitore":fornitore,
        "ivafornitore":iva,
        "quantita":quantita,
        "note":note,
        "dataingresso":data,
       
        "abilitato":"0",
        "codicesessione":codicesesione


       });
     });    
     
  }
  void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      var codicesessione=next.toInt();
    });
}
  

  List selectProduct = List();
  List etichetteProduct = List();
  List fornitori = List();
  List immagini=List();
  var lavori=<Map>[];
  String eti='nome etichetta ';
  String fornitoda=' da ';
  final members = new List<Member>();

  void delete(codice,prodotto){
  var elimino='ss';
  var lista= new List();
  print(prodotto);
  Map<String, dynamic> tes=new Map();

  Firestore.instance.collection("lavorazione")
  .where("codice", isEqualTo: codice)
  .getDocuments().then((string) {
    string.documents.forEach((doc) => 
    //doc.reference.delete(),
    doc.data.addAll(tes)
    

    //print("Firestore response2: ${doc.data.toString()}")
    );
    
 print(elimino);

    
  });
 

  }


  void elimina(cosa){
    Firestore.instance.runTransaction((transaction)async{
            DocumentSnapshot snapshot=
            await transaction.get(cosa);
            await transaction.delete(snapshot.reference);
          });
  }
  



 
   void _onCategorySelected(bool selected, category_id, nome, fornitore,pathimmagine, iva,quantita,note,data,codrandom) {
    if (selected == true) {
      //metodoinserimento nel DB
      //adddatacheck(nome,pathimmagine,fornitore,iva,quantita,note,data,category_id,codrandom);
      setState(() {
        selectProduct.add(category_id);
        etichetteProduct.add(nome);
        fornitori.add(fornitore);
        immagini.add(pathimmagine);
        

      });
    } else {
      //metodo cancellazione proodotti
      delete(codrandom,nome);
      setState(() {
        selectProduct.remove(category_id);
        etichetteProduct.remove(nome);
      });
    }
  }
  
  
   @override
   Widget build(BuildContext context){


     return Scaffold(

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            actions: <Widget>[
              FlatButton(
                onPressed: (){
                     Navigator.of(context).push(new MaterialPageRoute(
                          builder: (BuildContext context)=> new lavorazione(
                          
                           codicesessione: codicesesione,
                          )
                        )
                        );
                },
                child: Row(
                  children: <Widget>[
                    Text('ok')
                  ],
                ),
              )
            ],
            title: Text('Procedere con il salvataggio?'),
            content: Column(
              children: <Widget>[
               Container(
                   height: 300.0, // Change as per your requirement
                   width: 400.0, // Change as per your requirement
                 child: new ListView.builder(
    
    padding: const EdgeInsets.all(4.0),
    
    itemCount: document.length,
                        
    
    
    
    
     itemBuilder: (BuildContext context, int i) {
      
       
       String urlimmagine=document[i].data['pathimmagine'].toString();
       String etichetta= document[i].data['etichetta'].toString();
       String nomefornitore= document[i].data['nomefornitore'].toString();
       String ivafornitore= document[i].data['ivafornitore'].toString();
       String quantita= document[i].data['quantita'].toString();
       String note= document[i].data['note'].toString();
       String data= document[i].data['data'].toString();
       String pathimmagine=document[i].data['pathimmagine'].toString();
       int codrandom=document[i].data['codice'];
       var codice= Key(document[i].documentID);
       String value;
       bool checked;

        
      return new Card (
        child:
             ListTile(
              
              title: 
              
                  Row(
                    children: <Widget>[
                      Column(children: <Widget>[
                      Image.network(urlimmagine,width: 80,height: 80),
                      Text(codrandom.toString())
                      

              ],),
              Padding(
                  padding: const EdgeInsets.only(left:1.0),
                  child: Column(
                    
                         
                          children: <Widget>[
                            Text('Nome etichetta: '),
                            Text(etichetta),
                            Padding(
                              padding: const EdgeInsets.only(top:4.0),
                              child: Text('Data fornitura '),
                            ),
                            Text(data),
                            
                          ],
                        ),
              ),
                    ],
                  ), 
           onTap: (){
             adddatacheck(etichetta,pathimmagine,nomefornitore,ivafornitore,quantita,note,data);
           }, ),
        );
       
       
       
       



       
      








     }




  ),
               ),

              ],
            ),




          )
        );
      

       },
            
            
     ),
     
   


 
            
            
     



  body:
   ListView.builder(
    
    padding: const EdgeInsets.all(4.0),
    
    itemCount: document2.length,
                      
    
    
    
    
     itemBuilder: (BuildContext context, int i) {
      
       
       String urlimmagine=document2[i].data['pathimmagine'].toString();
       String etichetta= document2[i].data['materieprime'].toString();
       String nomefornitore= document2[i].data['nomefornitore'].toString();
       String ivafornitore= document2[i].data['ivafornitore'].toString();
       String quantita= document2[i].data['quantita'].toString();
       String note= document2[i].data['note'].toString();
       String data= document2[i].data['data'].toString();
       String pathimmagine=document2[i].data['pathimmagine'].toString();
       int codrandom=document2[i].data['codice'];
       var codice= Key(document2[i].documentID);
       String value;
       bool checked;

        
      return new Card (
        child:
             ListTile(
              
              title: 
              
                Row(
                  children: <Widget>[
                    Column(children: <Widget>[
                    Image.network(urlimmagine,width: 100,height: 130),
                    Text(codrandom.toString())
                    

              ],),
              Padding(
                padding: const EdgeInsets.only(left:16.0),
                child: Column(
                  
                       
                        children: <Widget>[
                          Text('Nome etichetta: '),
                          Text(etichetta),
                         
                          
                          
                        ],
                      ),
              ),
                  ],
                ) 
            ),
        );
       
       
       
       



       
      








     }




  ),
     );
  
  
  }

  _lavorazione createState() => new _lavorazione();
  
  

}




