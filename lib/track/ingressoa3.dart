import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressofinale.dart';
import 'dart:io';

//modello classe
import 'package:foodapp/track/model/etichette.dart';




class ingressoa3 extends StatefulWidget {

  ingressoa3({this.nome, this.iva, this.index, this.urlimmagine, this.pathimmagine, this.data, this.filename });
  final String nome;
  final String iva;
  final String data;
  final String filename;
  final index;
  final String urlimmagine;
  final String pathimmagine;

  @override
  _ingressoa3State createState() => _ingressoa3State();


}

class _ingressoa3State extends State<ingressoa3> {

  String nome;
  String iva;
  String pathimmagine;
  File image;
  String data;
  //String nomeetichetta;
  String filename;
  String urlimmagine;
  String nomeetichetta;




  List<Doctor> _list = new List();
  List<Doctor> searchresult = new List();
  String _searchText = "";
  String sortParam = 'nome';
  Widget appBarTitle = new Text(
    "Etichette",
  );
  Icon icon = new Icon(
    Icons.search,
  );
  final TextEditingController _controller = new TextEditingController();
  bool _isSearching;

  _ingressoa3State() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {


      nome=widget.nome;
      iva=widget.iva;
      pathimmagine=widget.pathimmagine;
      image=image;
      data=widget.data;
      filename=widget.filename;
      urlimmagine=widget.urlimmagine;

    super.initState();
    _isSearching = false;
    Firestore.instance
        .collection("etichette")
        .orderBy(sortParam, descending: true)
        .getDocuments()
        .then((querySnapshot) {
      List<DocumentSnapshot> documentSnapshot = querySnapshot.documents;
      for (DocumentSnapshot ds in documentSnapshot) {
        setState(() {
          _list.add(Doctor(
              ds.data['nome']));
          sortParam = 'nome';
        });
      }
    });
  }

  void searchOperation(String searchText) {
    searchresult.clear();
    if (_isSearching) {
      for (Doctor doc in _list) {
        if (doc.name.toLowerCase().contains(searchText)) searchresult.add(doc);
      }
    }
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

paginafinale(String nuovaetichetta){
    Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context)=> new ingressofinale(
                              nome: nome,
                              iva: iva,
                              pathimmagine: pathimmagine,
                              etichetta: nuovaetichetta,
                              data: data,
                              filename: filename,
                            )
                          )
                          );
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = new Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = new Text(
        "Etichette",
        style: new TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
      searchresult.clear();
    });
  }

  List<String> choices = <String>[
    "nome",
    "specialist",
    "name Desc",
    "specialist Desc"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Flexible(
                child: searchresult.length != 0 || _controller.text.isNotEmpty
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: searchresult.length,
                        itemBuilder: (BuildContext context, int index) {
                          Doctor currDoc = searchresult[index];
                          return Card(
                                                      child: Column(
                              children: <Widget>[
                                
                                
                 ListTile(
                                   
                                  
                                  title: Text(currDoc.name),


                                  //subtitle: Text(currDoc.specialist),
                                
                                
                                
                                ),

                                 new ButtonTheme.bar(
                  child: new ButtonBar(
                    children: <Widget>[
                      new FlatButton(
                        child: Row( children: <Widget>[
                          Icon(Icons.add_box),
                          Text('Seleziona',style: TextStyle(fontSize: 18),)
                        ], ),
                        onPressed: () { 

                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context)=> new ingressofinale(
                              nome: nome,
                              iva: iva,
                              pathimmagine: pathimmagine,
                              etichetta: currDoc.name,
                              data: data,
                              filename: filename,
                            )
                          )
                          );
                      


                        },
                      ),
                      
                    ],
                  ),
                ),
                              ],
                            ),
                          );
                        },
                      )
                    : _list.length != 0
                        ? ListView.builder(
                            shrinkWrap: true,
                            itemCount: _list.length,
                            itemBuilder: (BuildContext context, int index) {
                              Doctor currDoc = _list[index];
                              return Card(
                                                              child: Column(
                                  children: <Widget>[
                                    ListTile(
                                        title: Text(currDoc.name),
                                        
                                        
                                        
                                        
                                        ),

                                           new ButtonTheme.bar(
                  child: new ButtonBar(
                    children: <Widget>[
                      new FlatButton(
                        child: Row( children: <Widget>[
                          Icon(Icons.add_box),
                          Text('Seleziona',style: TextStyle(fontSize: 18),)
                        ], ),
                        onPressed: () { 

                          Navigator.of(context).push(new MaterialPageRoute(
                            builder: (BuildContext context)=> new ingressofinale(
                              nome: nome,
                              iva: iva,
                              pathimmagine: pathimmagine,
                              etichetta: currDoc.name,
                              data: data,
                              filename: filename,
                            )
                          )
                          );
                      


                        },
                      ),
                      
                    ],
                  ),
                ),


                                  ],
                                ),
                              );
                                 // subtitle: Text(currDoc.specialist));
                            },
                          )
                        : Center(
                            child: Text(
                              "Nessun record trovato",
                              style: TextStyle(
                                  color: Color.fromRGBO(119, 136, 153, 1.0)),
                            ),
                          )),
          ],
        ),
      ),
      appBar: AppBar(
        title: appBarTitle,
        actions: <Widget>[
          IconButton(
              icon: icon,
              onPressed: () {
                setState(() {
                  if (this.icon.icon == Icons.search) {
                    this.icon = Icon(Icons.close);
                    this.appBarTitle = TextField(
                      controller: _controller,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search,color: Colors.white,),
                        hintText: 'Cerca',
                        hintStyle: TextStyle(color: Colors.white),
                      ),
                      onChanged: searchOperation,
                    );
                    _handleSearchStart();
                  } else {
                    _handleSearchEnd();
                  }
                });
              }),
        
        //da eliminare


          


          //da eliminare sopra tasto ordina, in test lo lascio per futura applicazione
        ],
      ),
      floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.add),
       backgroundColor: Colors.blue,
       onPressed: () {

          showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Column(children: <Widget>[Text('Inserire una nuova etichetta'), 
            
            TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          nomeetichetta=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
            
            
            
            
             ],),
            
            actions: <Widget>[
              
              FlatButton(
                child: Text('Annulla',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  Navigator.pop(context);
                  
                   
                 
                  
                  
                },
              ),
              FlatButton(
                child: Text('Inserisci',style: TextStyle(fontSize: 14)),
                onPressed:(){
                
                Firestore.instance.runTransaction((Transaction transsaction) async{
                  CollectionReference reference= Firestore.instance.collection('etichette');
                  await reference.add({
                    "nome" : nomeetichetta,
                
                  }).whenComplete( paginafinale(nomeetichetta));
               
                });
                   
                  

                
                }
                

                   
                 
                  
                  
                
              )
            ],
          )
        );




       }
            
            
     ),
    );
  }
}