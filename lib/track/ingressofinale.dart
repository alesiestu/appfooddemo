import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:io';
import 'package:image/image.dart' as Im;
import 'dart:math' as Math;
import 'package:foodapp/track/ingressoa3.dart';
import 'package:foodapp/track/stadi.dart';
import 'dart:math';

class ingressofinale extends StatefulWidget{
  ingressofinale({this.nome, this.iva, this.index, this.urlimmagine, this.pathimmagine, this.etichetta, this.data, this.filename });
  final String nome;
  final String iva;
  final index;
  final String urlimmagine;
  final String pathimmagine;
  final String etichetta; 
  final String data;
  final String filename;
  

  @override
  _ingressofinaleState createState() => new _ingressofinaleState(); 
}


class _ingressofinaleState extends State<ingressofinale>{
String nome;
String iva;
String pathimmagine;
String etichetta;
String note='';
String quantita='';
String data;
String abilitato='0';
String lavorazione='0';
String filename='';
String urlimmagine;
String appoggio;
String lotto;
String scadenza;
int codicerandom;
 
 
 void numero() {
  var rng = new Random();
  
  var next = rng.nextDouble() * 1000000;
  while (next < 100000) {
    next *= 10;
  }
  print(next.toInt());
  setState(() {
      codicerandom=next.toInt();
    });
}



    void _addData(){
     Firestore.instance.runTransaction((Transaction transsaction) async{
       CollectionReference reference= Firestore.instance.collection('ingresso');
       await reference.add({
         "nomefornitore" : widget.nome,
         "ivafornitore": widget.iva,
         "note":note,
         "pathimmagine":appoggio,
         "etichetta":etichetta,
         "quantita":quantita,
         "data":data,
         "abilitato":abilitato,
         "lavorazione":lavorazione,
         "filename": filename,
         "codice":codicerandom,
         "lotto":lotto,
         "scadenza":scadenza,
         "codicelotto":"$data/$lotto"

         

       });
      
       
     });

    
     

  }
  @override
    void initState() {
      // TODO: implement initState
      super.initState();
      nome=widget.nome;
      iva=widget.iva;
      etichetta=widget.etichetta;
      pathimmagine=widget.pathimmagine;
      data=widget.data;
      filename=widget.filename;
      printUrl();
      numero();
      
     
          
     
     
    }

   

    printUrl() async {
    StorageReference ref = 
        FirebaseStorage.instance.ref().child(filename);
    String urlimmagine = (await ref.getDownloadURL()).toString();
     setState(() {
        appoggio=urlimmagine;
        
     
          });
    print(urlimmagine);
    
    
     }





@override
    Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding : false,
      appBar: new AppBar(
        
        title: Text('Riepilogo',style: TextStyle(fontWeight: FontWeight.bold),),
        actions: <Widget>[
        ],
      ),

       floatingActionButton: new FloatingActionButton(
       child: Icon(Icons.save),
       backgroundColor: Colors.blue,
       onPressed:(){
        showDialog<String>(
          context: context,
          builder: (BuildContext context)=> AlertDialog(
            title: Text('Procedere con il salvataggio?'),
            actions: <Widget>[
              FlatButton(
                child: 
                Text('Cancella',style: TextStyle(fontSize: 14),),
                onPressed: (){
                  Navigator.pop(context,'No');
                },
              ),
              FlatButton(
                child: Text('Si',style: TextStyle(fontSize: 14)),
                onPressed: (){
                  _addData();
                   Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (BuildContext context) => new stadi(),
                        ),
                        ModalRoute.withName('/'));
                  
                  
                  
                },
              )
            ],
          )
        );
      

       },
            
            
     ),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
     bottomNavigationBar: new BottomAppBar(
       elevation: 20,
       color: Colors.grey,
       child: ButtonBar(
         children: <Widget>[],
       ),
     ),

      body:
      ListView(
         shrinkWrap: true,
              padding: EdgeInsets.all(15.0),
              children: <Widget>[

       
      Padding(
        padding: const EdgeInsets.only(top:8.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top:8.0),
              child: 
              
              Card(

                child: 
                
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                 
                  
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Ulteriori dettagli', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                  ),
                    Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Lotto:',style: TextStyle(fontSize: 18)),
                  ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          lotto=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
                   ),
                   Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Data di scadenza: ',style: TextStyle(fontSize: 18)),
                  ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          scadenza=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
                   ),

                  

                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('Quantità:',style: TextStyle(fontSize: 18)),
                  ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          quantita=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                      
                

                              ),
                            ),
                   ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Note aggiutive:',style: TextStyle(fontSize: 18)),
                            ),
                   Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: TextField(
                      
                      onChanged: (String str){
                        setState(() {
                          note=str; 
                                
                                          });
                        },
                        decoration: new InputDecoration(
                
                
                         
                

                              ),
                            ),
                   ),
                                  
                                ],
                              ),
              ),
            ),
            Card(
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text('Dettagli fornitore', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold), ),
                    subtitle: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:8.0),
                          child: Text('Nome : $nome',style: TextStyle(fontSize: 18), ),
                        ),
                        Text('Partita iva : $iva', style: TextStyle(fontSize: 18), )
                      ],
                    )
                  ),
                  
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top:16.0),
              child: Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text('Dettagli lotto', style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold)),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top:16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              
                              children: <Widget>[
                                 Image.file(File(pathimmagine),width: 200, height: 200,),
                                 
                                 Text('Etichetta: $etichetta', style: TextStyle(fontSize: 20),),

                              ],
                            ),
                          
                           
                          ],
                        ),
                      )
                    ),
                    
                  ],
                ),
              ),
            ),
          
            
          ],
        ),
      )

      ])


   
      

       


        
           

         


          
          
        
     
      
    
    
    );
    
    }


}
