import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

String myText1 = 'temp1';
String myText2 = 'temp2';
String myText3 = 'temp3';
String myText4 = 'temp4';
String myText5 = 'temp5';
String myText6 = 'temp6';
StreamSubscription<DocumentSnapshot> subscription;
final DocumentReference documentReference =
    Firestore.instance.document("company/Nova");

class Clean extends StatefulWidget {
  @override
  _CleanState createState() => _CleanState();
}

class _CleanState extends State<Clean> {
  @override
  void initState() {
    super.initState();
    subscription = documentReference.snapshots().listen((datasnapshot) {
      //FINDING A SPECIFICDOCUMENT IS EXISTING INSIDE A COLLECTION

      if (datasnapshot.exists) {
        setState(() {
          myText1 = "Document exist";
        });
      } else if (!datasnapshot.exists) {
        setState(() {
          myText2 = "Document not exist";
        });
      }

      //FINDING A SPECIFIC KEY IS EXISTING INSIDE A DOCUMENT

      if (datasnapshot.data.containsKey("name")) {
        setState(() {
          myText3 = "key exists";
        });
      } else if (!datasnapshot.data.containsKey("name")) {
        setState(() {
          myText4 = "key not exists";
        });
      }


      //FINDING A SPECIFIC VALUE IS EXISTING INSIDE A DOCUMENT

      if (datasnapshot.data.containsValue("nova")) {
        setState(() {
          myText5 = "value exists";
        });
      } else if (!datasnapshot.data.containsValue("nova")) {
        setState(() {
          myText6 = "value not exists";
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Text(myText1),
        new Text(myText2),
        new Text(myText3),
        new Text(myText4),
        new Text(myText5),
        new Text(myText6),
      ],
    );
  }
}