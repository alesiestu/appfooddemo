import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodapp/track/ingressoa2.dart';
import 'package:foodapp/haccp/newcontrollo.dart';
import 'package:foodapp/haccp/resolutiondettagli.dart';


class resolution extends StatefulWidget{
  
  @override
  _resolutionState createState() => new _resolutionState(); 
}

class _resolutionState extends State<resolution>{
 

    @override
    Widget build(BuildContext context) {
    return Scaffold(

      appBar: new AppBar(
        title: Text('Conformità da correggere'),
    
        actions: <Widget>[

          
        ],
      ),

       body: 
     new Stack(
     children: <Widget>[ 
     
     
     
     new Padding(
            padding: const EdgeInsets.only(top:30),
            child:
     
     StreamBuilder(
       stream: Firestore.instance
       .collection("controllogiornaliero").where("conformità", isEqualTo: 'no')
       .snapshots(),
      
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot ){
        if(!snapshot.hasData)
        return new Container( child: Center(
          child: CircularProgressIndicator()
        ),

        );
      return new listaclienti(document: snapshot.data.documents,);
      },

      
      
     ),
     ),
      
     ]
    )




    );
    }

}

class listaclienti extends StatelessWidget {
  
  listaclienti({this.document, this.nome,this.iva,this.path});
  final List<DocumentSnapshot> document;
  final nome;
  final iva;
  final path;

  @override
  Widget build(BuildContext context){

    
      return ListView.builder(
          itemCount: document.length,
          
          itemBuilder: (BuildContext context, int i) {
          
            
         
            String nome= document[i].data['nome'].toString();
            String domanda= document[i].data['macchinario'].toString();
            String pathimmagine= document[i].data['pathimmagine'].toString();
            
            String data=document[i].data['data'].toString();
            

             
    return new Padding(padding: new EdgeInsets.all(10.0),
        child: new Card(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: Column(
                  children: <Widget>[
                    new Text(
                       nome.toUpperCase()
                    ,style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold ),
                    ),
                  ],
                ),
                subtitle: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top:8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.network(pathimmagine,width: 300,height: 300),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Nome: ', style: TextStyle(fontSize: 16),),
                            ),
                            Text(domanda.toUpperCase(), style: TextStyle(fontSize: 18)),
                             Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Data controllo: ', style: TextStyle(fontSize: 16),),
                            ),
                            Text(data.toUpperCase(), style: TextStyle(fontSize: 18)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
               
                ])
                
                
              ),
              new ButtonTheme.bar(
                child: new ButtonBar(
                  children: <Widget>[
                    new FlatButton(
                      
                      child: Row( children: <Widget>[
                        Icon(Icons.add_box),
                        Text('Risolvi',style: TextStyle(fontSize: 18),)
                      ], ),
                      
                      onPressed: () { 
                        Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) => new risoluzionedettagli(
                        idmacchina: document[i].reference,
                        controllo: domanda,
                        pathimmagine: pathimmagine,


                      )),
                    );
                      
                      },
                    )
                      
                    
                  ],
                ),
              ),
            ],
          ),
        )
    );
  





            
          });

   

    
        
  
    
    
    
    
    
    
    
  
  }
  _resolutionState createState() => new _resolutionState();
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}
