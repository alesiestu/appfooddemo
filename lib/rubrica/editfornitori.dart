import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class editfornitori extends StatefulWidget {
  editfornitori({ this.nome, this.telefono, this.iva, this.indirizzo, this.index});
  
  final String nome;
  final String telefono;
  final String indirizzo;
  final String iva;
  final index;
  @override
  _editfornitoriState createState() => new _editfornitoriState();
}

class _editfornitoriState extends State<editfornitori> {
  
  TextEditingController controllernome;
  TextEditingController controllertelefono;
  TextEditingController controllerindirizzo;
  TextEditingController controlleriva;
  String nome;
  String telefono;
  String iva;
  String indirizzo;
  
  
  
  void updateclienti(){
  
    Firestore.instance.runTransaction((Transaction transaction)async {
      DocumentSnapshot snapshot=
      await transaction.get(widget.index);
      await transaction.update(snapshot.reference, {
        "nome": nome,
        "indirizzo":indirizzo,
        "telefono":telefono,
        "iva":iva

      });
    });
    Navigator.pop(context);

  }




  

 
  


  
   @override
     void initState() {
       nome=widget.nome;
       indirizzo=widget.indirizzo;
       telefono=widget.telefono;
       iva=widget.iva;

       // TODO: implement initState
       super.initState();
       controllernome=new TextEditingController(text: widget.nome);
       controllerindirizzo= new TextEditingController(text: widget.indirizzo);
       controllertelefono=new TextEditingController(text: widget.telefono);
       controlleriva=new TextEditingController(text: widget.iva);
       
     }

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: Column(
        children: <Widget>[
          Container(
            height: 200.0,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.blue
            ),
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
            Text("Modifica fornitore",style: new TextStyle(
              color: Colors.white,
              fontSize: 30.0,
              letterSpacing: 2.0,
              fontFamily: "Pacifico"
            ),)
          ],
          ),
          ),
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controllernome,
              onChanged: (String str){
                setState(() {
                 nome=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.dashboard),
                hintText: "Nome - Ragione sociale",
                border: InputBorder.none

              ),
            ),
          ),
          
          new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controllerindirizzo,
              onChanged: (String str){
                setState(() {
               indirizzo=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Indirizzo",
                border: InputBorder.none

              ),
            ),
          ),

           new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              keyboardType: TextInputType.number,
              controller: controllertelefono,
              onChanged: (String str){
                setState(() {
                telefono=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Telefono",
                border: InputBorder.none

              ),
            ),
          ),

           new Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextField(
              controller: controlleriva,
              onChanged: (String str){
                setState(() {
               iva=str;        
                                });
              },
              decoration: new InputDecoration(
                icon: Icon(Icons.note),
                hintText: "Partita iva",
                border: InputBorder.none

              ),
            ),
          ),

          new Padding(
            padding: const EdgeInsets.only(top:100),
            child:
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.check, size: 40.0,),
                onPressed: (){
                  updateclienti();
                 
                } ,
              ),
              IconButton(
                icon: Icon(Icons.close, size: 40.0,),
                onPressed: (){
                  Navigator.pop(context);
                } ,
              )
            ],
          )
          ),











        ],
      ),
      
    );
  }



  
   



  
}